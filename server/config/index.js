"use strict";
Object.defineProperty(exports, "__esModule", {
	value: true
});

function getConfig(key) {
	const config = {
		port: 3200,
		mysql_config: {
			dialect: 'mysql',
			host: 'localhost', // 数据库
			port: 3306,
			username: 'cst',
			password: 'XX4WJ',
			database: 'cst',
			timezone: '+08:00',
			dialectOptions: {
				dateStrings: true,
				typeCast: true
			}
		},
		redis_config: {
			type: 'redis',
			host: '0.0.0.0',
			port: 6381,
			password: 'ret'
		},
		email: 'qq1161839630@163.com',
		email_config: {
			host: 'smtp.163.com',
			port: 25,
			ignoreTLS: true,
			secure: false,
			auth: {
				user: '',
				pass: '' // 密码
			}
		}
	};
	if (key) {
		return config[key];
	}
	return config;
}
exports.default = {
	getConfig
};
//# sourceMappingURL=index.js.map
