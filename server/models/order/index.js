"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mysql_1 = tslib_1.__importDefault(require("./mysql"));
async function getOrders({ page, page_size }, where) {
    const finds = await mysql_1.default.findAndCountAll({
        where,
        order: [['create_time', 'DESC']],
        offset: page * page_size,
        limit: page_size
    });
    return finds;
}
async function getOrderInfo(id) {
    const find = await mysql_1.default.findByPk(id);
    if (!find)
        return null;
    return find.toJSON();
}
async function delOrder(id) {
    const del = await mysql_1.default.destroy({
        where: {
            id
        }
    });
    return del;
}
async function addOrder(data) {
    const add = await mysql_1.default.create(data);
    return add;
}
async function editOrder(data) {
    const edit = await mysql_1.default.upsert(data);
    return edit;
}
exports.default = {
    getOrders,
    delOrder,
    addOrder,
    editOrder,
    getOrderInfo
};
//# sourceMappingURL=index.js.map