"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mysql_1 = tslib_1.__importDefault(require("./mysql"));
async function addTurnover(data) {
    const create = await mysql_1.default.create(data);
    return create;
}
async function getUserTurnovers({ page, page_size }, where) {
    const finds = await mysql_1.default.findAndCountAll({
        where,
        order: [['create_time', 'DESC']],
        offset: page * page_size,
        limit: page_size
    });
    return finds;
}
async function getTurnovers({ page, page_size }, where) {
    const find = await mysql_1.default.findAndCountAll({
        where,
        order: [['create_time', 'DESC']],
        offset: page * page_size,
        limit: page_size
    });
    return find;
}
async function delTurnover(id) {
    const del = await mysql_1.default.destroy({
        where: {
            id
        }
    });
    return del;
}
async function editTurnover(data) {
    const res = await mysql_1.default.upsert({
        ...data
    });
    return res;
}
exports.default = {
    addTurnover,
    getUserTurnovers,
    delTurnover,
    getTurnovers,
    editTurnover
};
//# sourceMappingURL=index.js.map