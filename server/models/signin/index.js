"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const db_1 = require("../db");
const mysql_1 = tslib_1.__importDefault(require("./mysql"));
async function addSignin(data) {
    const create = await mysql_1.default.create({
        ...data
    });
    return create;
}
async function getUserDaySignin(user_id, time) {
    const create = await mysql_1.default.findOne({
        where: {
            user_id,
            create_time: {
                [db_1.sequelize.Op.gte]: time
            }
        }
    });
    return create;
}
async function getSignins({ page, page_size }, where) {
    const find = await mysql_1.default.findAndCountAll({
        where,
        order: [['create_time', 'DESC']],
        offset: page * page_size,
        limit: page_size
    });
    return find;
}
exports.default = {
    addSignin,
    getUserDaySignin,
    getSignins
};
//# sourceMappingURL=index.js.map