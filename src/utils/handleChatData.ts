export function handleChatData(text: string | undefined) {
  if (!text) {
    return [];
  }
  const data: any[] = [];
  const errors: any[] = [];
  text.split('\n\n')
    .filter(item => item.trim() !== '')
    .forEach(d => {
      try {
        const obj = JSON.parse(d.trim());
        data.push(obj);
      } catch (e) {
        // console.error(`Failed to parse JSON: ${d.trim()}`, e);
        errors.push(d.trim());
      }
    });
  // console.log(`Failed to parse ${errors.length} JSON objects:`, errors);
  return data;
}