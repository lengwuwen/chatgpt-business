<div align="center">
<img src="./src/assets/openai.svg" style="width:64px;height:64px;margin:0 32px" alt="icon"/>

<h1 align="center">ChatGPT Web</h1>


A commercially-viable ChatGpt web application built with React.

可部署商业化的 ChatGpt 网页应用。

![Deploy to Vercel](https://vercel.com/button)

</div>

## 🐶 演示
### 页面链接


如需帮助请联系VX:dadanaoken

### 页面截图

![cover](https://files.catbox.moe/tp963e.png)
![cover](https://files.catbox.moe/y5avbx.png)
![cover](https://files.catbox.moe/k16jsz.png)
![cover](https://files.catbox.moe/8o5oja.png)

## 🤖 主要功能

- 后台管理系统,可对用户,Token,商品,卡密等进行管理
- 精心设计的 UI，响应式设计
- 极快的首屏加载速度（~100kb）
- 支持Midjourney绘画和DALL·E模型绘画,GPT4等应用
- 海量的内置 prompt 列表，来自[中文](https://github.com/PlexPt/awesome-chatgpt-prompts-zh)和[英文](https://github.com/f/awesome-chatgpt-prompts)
- 一键导出聊天记录，完整的 Markdown 支持
- 支持自定义API地址（如：[openAI](https://api.openai.com) / [API2D](https://api2d.com/r/192767)）

## 🎮 开始使用
**Node 环境**

`node` 需要 `^16 || ^18 || ^19` 版本（node >= 16.19.0），可以使用 nvm 管理本地多个 node 版本。

```
# 查看 node 版本
node -v

# 查看 npm 版本
npm -v

# 查看 yarn 版本
yarn -v

```

**1.先 `Fork` 本项目，然后克隆到本地。**
```
git clone https://gitee.com/lengwuwen/chatgpt-business.git
```

**2.安装依赖**
```
yarn install
```

**3.运行**
```
# web项目启动
yarn dev:web
```

**4.打包**
```
yarn build
```

## ⛺️ 环境变量

> 本项目大多数配置项都通过环境变量来设置。

#### `VITE_APP_REQUEST_HOST` 

请求服务端的`Host`地址。

#### `VITE_APP_TITLE` 

Chat Web 标题名称。

#### `VITE_APP_LOGO` 

Chat Web Logo。

## 🚧 开发

> 强烈不建议在本地进行开发或者部署，由于一些技术原因，很难在本地配置好 OpenAI API 代理，除非你能保证可以直连 OpenAI 服务器。

#### 本地开发

1. 安装 nodejs 和 yarn具体细节请询问 ChatGPT
2. 执行 `yarn install` 即可
3. web项目开发 `yarn dev:web`
4. 服务端项目开发 `yarn dev`
5. 打包项目 `yarn build`

## 🎯 部署
> 直接将`WEB`项目打包好的 `dist` 目录上传到服务器即可。注意服务器IP地址位置！

### Vercel
如果你将其托管在自己的 Vercel 服务器上，可点击 deploy 按钮来开始你的部署！

[![Deploy to Vercel](https://vercel.com/button)]

如需帮助请提交 Issues。
